#!/bin/bash -e
# Generate reports on the Stream composes.
# This is roughly a jenkins version of ./cron-compose-reporting.sh

git clone github.com:CentOS/sync2git.git
cd sync2git

# perm. storage is ~/ so we can just use it for:
# ~/.local/composes

if [ ! -d ~/.local/composes ]; then
 # We are running for the first time...
 ./compose.py add c8s https://composes.centos.org/latest-CentOS-Stream-8/
 ./compose.py add c9s https://composes.stream.centos.org/production/latest-CentOS-Stream
 ./compose.py add c9s-dev https://composes.stream.centos.org/development/latest-CentOS-Stream
 ./compose.py add fedora-eln https://odcs.fedoraproject.org/composes/production/latest-Fedora-ELN/
 ./compose.py add rhel8 http://download.eng.bos.redhat.com/composes/nightly-rhel-8/RHEL-8/latest-RHEL-8/
 ./compose.py add rhel9 http://download.eng.bos.redhat.com/composes/nightly-rhel-9/RHEL-9/latest-RHEL-9/

 if [ "$(date +'%s')" -le "16366600000" ]; then
   ./compose.py _dl c9s CentOS-Stream-9-20211104.1
   ./compose.py _dl c9s CentOS-Stream-9-20211105.1
   ./compose.py _dl c9s CentOS-Stream-9-20211108.1
   ./compose.py _dl c9s CentOS-Stream-9-20211109.1

   ./compose.py _dl rhel9 RHEL-9.0.0-20211018.7
   ./compose.py _dl rhel9 RHEL-9.0.0-20211019.7
   ./compose.py _dl rhel9 RHEL-9.0.0-20211020.3
   ./compose.py _dl rhel9 RHEL-9.0.0-20211108.7
 fi

 exit 0
fi

#  Run ./cron-compose-reporting.py eventually. For now just do the
# updates:

./compose.py check
./compose.py clean

