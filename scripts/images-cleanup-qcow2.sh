#!/usr/bin/bash

# Variables
#base_link_dir="/mnt/centos/koji/packages"
base_link_dir="../../mnt/centos/koji/packages"
CENTOS_RELEASE="9-stream"

trim_images() {
    arch=$1
    echo "Checking ${arch} images"
    # Setup
    mkdir -p stage/images/
    rm -f  stage/images/*
    pushd stage/images/

    ## Sync the current centos.cloud to us
    ##  To save space and network, soft link the images from their local areas
    # First get list of files in directory
    echo "  Getting current repo from centos cloud"
    wget -q -O index.html  https://cloud.centos.org/centos/9-stream/${arch}/images/
    # Create symlinks to all the large files
    grep href= index.html | sed "s|^.*href=||" | cut -d '>' -f2 | cut -d'<' -f1 | grep ^CentOS-Stream | grep -v -e MD5SUM$ -e SHA1SUM$ -e SHA256SUM$ -e CHECKSUM | while read fileName
    do
      buildNum=$(echo ${fileName} | sed "s/^.*-9-//" | sed "s/.${arch}.*//")
      buildTypeTmp=$(echo ${fileName} | cut -d'-' -f1-3)
      if [ "${buildTypeTmp}" == "CentOS-Stream-Container" ] ; then
          buildType="CentOS-Stream-Container-Base"
      elif [ "${buildTypeTmp}" == "CentOS-Stream-ec2" ] && [ "${arch}" == "aarch64" ] ; then
          buildType="CentOS-Stream-ec2-aarch64"
      else
          buildType="${buildTypeTmp}"
      fi
      ln -s ${base_link_dir}/${buildType}/9/${buildNum}/images/${fileName} .
    done
    # # Pull down the files we haven't linked to, from cloud.centos
    rsync -avhH --info progress --ignore-existing \
        centos@master-1.centos.org:/home/centos-cloud/centos/${CENTOS_RELEASE}/${arch}/images/ \
        ./
    # Get rid of index.html
    rm -f index.html

    ## Clean things up
    echo "  Trimming out old images"
    # Find the date for the last 12 months ago
    declare -A testd
    testd["lastmonth"]=$(date --date="last month"  +%Y%m%d)
    let mon=2
    while [ ${mon} -le 12 ] ; do
      testd["${mon}month"]=$(date --date="${mon} months ago"  +%Y%m%d)
      let mon=${mon}+1
    done
    # Work through the files
    ls -1  | grep -v -e MD5SUM$ -e SHA1SUM$ -e SHA256SUM$ -e CHECKSUM | while read fileName ; do
      buildNum=$(echo ${fileName} | sed "s/^.*-9-//" | sed "s/.${arch}.*//")
      buildDate=$(echo ${buildNum} | cut -d'.' -f1)
      # echo "    Checking: ${fileName}"
      if [ ${buildDate} -lt ${testd["lastmonth"]} ] ; then
          keep="False"
          let mon=2
          while [ ${mon} -le 12 ] ; do
              if [ ${buildDate} -ge ${testd["${mon}month"]} ] ; then
                  if [ ! ${testd["${mon}file"]} ] || [ ${buildDate} -eq ${testd["${mon}file"]} ] ; then
                      keep="True"
                      testd["${mon}file"]=${buildDate}
                  fi
                  break
              fi
              let mon=${mon}+1
          done
          if [ "${keep}" == "False" ] ; then
            rm -f *${buildNum}*
            echo "      DELETE: *${buildNum}*"
          fi
      fi
    done

    ## Update checksum with only the images left
    rm -f CHECKSUM
    cat *.SHA256SUM > CHECKSUM

    ## Done with cleanup, sync it all back, deleting what was removed
    echo "  Syncing everything back"
    rsync -avhH --info progress --copy-links --delete -n \
        ./ \
        centos@master-1.centos.org:/home/centos-cloud/centos/${CENTOS_RELEASE}/${arch}/images/

    ## Cleanup
    popd
    rm -rf stage/images/

}

trim_images aarch64
trim_images ppc64le
trim_images s390x
trim_images x86_64
